package Entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import Helpers.AssetsLoader;
import Helpers.Constants;
import Helpers.CustomUserData;
import Helpers.LevelManager;

public class Treasure extends GameObjects{

		public Treasure(World world) {
			for (MapObject object: LevelManager.whatLVL().getLayers().get(6).getObjects().getByType(RectangleMapObject.class))
	             rect = ((RectangleMapObject) object).getRectangle();
	             

			BodyDef bdef = new BodyDef();  
	        bdef.position.set((rect.getX() + rect.getWidth() / 2) / Constants.PPM,( rect.getY() + rect.getHeight() / 2) / Constants.PPM);
	        bdef.type = BodyDef.BodyType.StaticBody;
	        b2body = world.createBody(bdef);
	        
	        FixtureDef fdef = new FixtureDef();
	        CircleShape shape = new CircleShape();
	        shape.setRadius(15f / Constants.PPM);
	        
	        fdef.shape = shape;
	        b2body.createFixture(fdef).setUserData("treasure");
	        b2body.setUserData(new CustomUserData(false, "treasure"));
	        
	        isAlive = true;
	        
	}
	
	
	@Override
	public void draw(SpriteBatch batcher) {

		if (b2body.getUserData() != null)
		{
		sprite = new Sprite(AssetsLoader.getInstance().getTreasureImg());
        sprite.setSize(1, 1);
        sprite.setPosition((b2body.getPosition().x - sprite.getWidth()/2), ((b2body.getPosition().y - sprite.getHeight()/2)));
        sprite.draw(batcher);
		}
		
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

}
