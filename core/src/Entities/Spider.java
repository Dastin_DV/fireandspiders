package Entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import Helpers.AssetsLoader;
import Helpers.Constants;
import Helpers.CustomUserData;

public class Spider extends GameObjects {

	private boolean isActive;
	private MainHero mainHero;
	private boolean madeOneStep;
	private static boolean deleteFact;
	
	public Spider(World world, Rectangle rect, MainHero mainHero) {
		this.mainHero = mainHero;
		BodyDef bdef = new BodyDef();  
        bdef.position.set((rect.getX() + rect.getWidth() / 2) / Constants.PPM,( rect.getY() + rect.getHeight() / 2) / Constants.PPM);
        bdef.type = BodyDef.BodyType.StaticBody;
        b2body = world.createBody(bdef);
        
        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(15f / Constants.PPM);
        
        fdef.shape = shape;
        b2body.createFixture(fdef).setUserData("spider");
        b2body.setUserData(new CustomUserData(false, "spider"));
        
        isActive = false;
        isAlive = true;
        madeOneStep = false;
	}
	

    public void checkForMHActivities()
    {
    	if ((Math.abs((mainHero.getY() - b2body.getPosition().y)) < 0.1f && Math.abs((mainHero.getX() - b2body.getPosition().x)) <= 2.2f) ||
                (Math.abs(mainHero.getY() - b2body.getPosition().y) <=2.2f && Math.abs((mainHero.getX() - b2body.getPosition().x)) <= 0.1f))
         {
    		 System.out.println("ACTIVE!"); 
    		 AssetsLoader.getInstance().getSpiderActiveSound().play();
             isActive = true;
         }
    	
    	if (Math.abs((mainHero.getY() - b2body.getPosition().y)) <= 0.1f && (mainHero.getX() - b2body.getPosition().x) < 0 &&  Math.abs((mainHero.getX() - b2body.getPosition().x)) <= 2.2f)
        {           
    		b2body.setTransform(new Vector2(b2body.getPosition().x - 1, b2body.getPosition().y), 0);
    		madeOneStep = true;
        }
    	
    	if (Math.abs((mainHero.getY() - b2body.getPosition().y)) <= 0.1f && (mainHero.getX() - b2body.getPosition().x) > 0
    			&&  Math.abs((mainHero.getX() - b2body.getPosition().x)) <= 2.2f)
        {           
    		b2body.setTransform(new Vector2(b2body.getPosition().x + 1, b2body.getPosition().y), 0);
    		madeOneStep = true;
        }
    	
    	if ((mainHero.getY() - b2body.getPosition().y) < 0 && Math.abs((mainHero.getX() - b2body.getPosition().x)) <= 0.1f
    			&&  Math.abs((mainHero.getY() - b2body.getPosition().y)) <= 2.2f)
        {           
    		b2body.setTransform(new Vector2(b2body.getPosition().x, b2body.getPosition().y - 1), 0);
    		madeOneStep = true;
        }
    	
    	if ((mainHero.getY() - b2body.getPosition().y) > 0 && Math.abs((mainHero.getX() - b2body.getPosition().x)) <= 0.1f
    			&&  Math.abs((mainHero.getY() - b2body.getPosition().y)) <= 2.2f)
        {           
    		b2body.setTransform(new Vector2(b2body.getPosition().x, b2body.getPosition().y + 1), 0);
    		madeOneStep = true;
        }


    }

    
    public boolean checkForGameOver()
    {
         if (b2body!=null && Math.abs(mainHero.getX() - b2body.getPosition().x) <= 0.01f && Math.abs(mainHero.getY() - b2body.getPosition().y) <= 0.01f)
        {
                if (mainHero.getSwordCount() > 0)
                {
	                 if (b2body!=null && b2body.getUserData()!=null)
	                 { 
	                   CustomUserData userData =  (CustomUserData) b2body.getUserData();
	                   userData.setDeleteFlagTrue();
	                   b2body.setUserData(userData);
	                   isKilled = true;
	                   
	                   // ���� �������� - ������� ������� ������ ���������. ����� ����.
	                   registerDeleteFact();
	                 }                    
                }
                else
                {
                    return true;
                }
        }
        return false;
    }
 
    public boolean isActive() {
    	return isActive;
    }
    
    public boolean isMadeStep() {
    	return madeOneStep;
    }
    
    public void allowMakeStep() {
    	madeOneStep = false;
    }
    
    private void registerDeleteFact() {
    	deleteFact = true;
	}
    
    public static boolean getDeleteFact() {
    	return deleteFact;
	}
    
    public static void resetDeleteFact() {
    	deleteFact = false;
	}
    
	@Override
	public void draw(SpriteBatch batcher) {	
		
		if (b2body.getUserData() != null)
		{
		sprite = new Sprite(AssetsLoader.getInstance().getSpiderImg());
        sprite.setSize(1, 1);
        sprite.setPosition((b2body.getPosition().x - sprite.getWidth()/2), ((b2body.getPosition().y - sprite.getHeight()/2)));
        sprite.draw(batcher);
		}

	}
	
	@Override
	public void update(float delta)
	{
		if (isActive)
		{
			b2body.setTransform(mainHero.getLastPosition(), 0);
			madeOneStep = true;
		}
			
	}

}
