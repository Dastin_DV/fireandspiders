package Entities;

import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import Helpers.AssetsLoader;
import Helpers.Constants;
import Helpers.CustomUserData;
import Helpers.LevelManager;

public class MainHero extends GameObjects{
    
	private float beginXpos;
	private float beginYpos;
	private boolean mLeft;
	private boolean mRight;
	private boolean mUp;
	private boolean mDown;
	private boolean isMoving;
	private Vector2 lastPosition;
	private float fallibility;
	private int coinCount = 0;
	private int swordCount = 0;
	private Timer timer;
	private float lastPressedButtonTime;
	
	public MainHero(World world) {
		BodyDef bdef = new BodyDef();
		Rectangle rect = null;
        for (MapObject object: LevelManager.whatLVL().getLayers().get(1).getObjects().getByType(RectangleMapObject.class))
        	 rect = ((RectangleMapObject) object).getRectangle();
        
        bdef.position.set((rect.getX() + rect.getWidth() / 2) / Constants.PPM,( rect.getY() + rect.getHeight() / 2) / Constants.PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);
        
        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(15f / Constants.PPM);
        fdef.shape = shape;
        b2body.createFixture(fdef).setUserData("main");
        
        EdgeShape foot = new EdgeShape();
        foot.set(new Vector2(-11 / Constants.PPM, -11 / Constants.PPM) , new Vector2(11/ Constants.PPM, 11/ Constants.PPM));
        fdef.shape = foot;
        b2body.createFixture(fdef).setUserData("main");
        b2body.setUserData(new CustomUserData(false, "main"));
        fixture = b2body.createFixture(fdef);
        
        isMoving = false;
        lastPosition = new Vector2(0,0);
        
        timer = new Timer();
	}
	
	public void toAllowMovement() {
		isMoving = false;
	}
	
	public void setUp() {
		mUp = true;
		mLeft = false;
		mRight = false;
		mDown = false;
		isMoving = true;
	}
	
	public void setDown() {
		mDown = true;
		mLeft = false;
		mRight = false;
		mUp = false;
		isMoving = true;
	}
	
	public void setLeft() {
		mLeft = true;
		mRight = false;
		mDown = false;
		mUp = false;
		isMoving = true;
	}
	
	public void setRight() {
		mRight = true;
		mUp = false;
		mLeft = false;
		mDown = false;
		isMoving = true;
	}
	
	
	private void handleInput(float delta) {
		
		if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
			setUp();
			beginYpos = b2body.getPosition().y;
			lastPosition.x = b2body.getPosition().x;
			lastPosition.y = b2body.getPosition().y;
            b2body.setLinearVelocity(0, 3);
            lastPressedButtonTime = TimeUtils.nanoTime();
        }
		
		if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
			setDown();
			beginYpos = b2body.getPosition().y;
			lastPosition.x = b2body.getPosition().x;
			lastPosition.y = b2body.getPosition().y;
			b2body.setLinearVelocity(0,-3);
			lastPressedButtonTime = TimeUtils.nanoTime();
		}
		
		if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
			setLeft();
			beginXpos = b2body.getPosition().x;
			lastPosition.x = b2body.getPosition().x;
			lastPosition.y = b2body.getPosition().y;
			b2body.setLinearVelocity(-3, 0);
			lastPressedButtonTime = TimeUtils.nanoTime();
		}
		
		if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
			setRight();
			beginXpos = b2body.getPosition().x;
			lastPosition.x = b2body.getPosition().x;
			lastPosition.y = b2body.getPosition().y;
			b2body.setLinearVelocity(3, 0);
			lastPressedButtonTime = TimeUtils.nanoTime();
		}	
		
	}

	private void makeOneStep() {
		if (mLeft) {
			if ((int)(beginXpos - b2body.getPosition().x) == 1) {
				b2body.setLinearVelocity(0,0);
				fallibility = Math.abs(lastPosition.x - b2body.getPosition().x - 1);
				b2body.setTransform(new Vector2(b2body.getPosition().x + fallibility, b2body.getPosition().y),0);
				isMoving = false;
			}
		}
		
		if (mRight) {
			if ((int)(beginXpos - b2body.getPosition().x) == -1) {
				b2body.setLinearVelocity(0,0);	
				fallibility = Math.abs(lastPosition.x - b2body.getPosition().x) - 1;
				b2body.setTransform(new Vector2(b2body.getPosition().x - fallibility, b2body.getPosition().y),0);
				isMoving = false;
			}
		}
		
		if (mUp) {
			if ((int)(beginYpos - b2body.getPosition().y) == -1) {
				b2body.setLinearVelocity(0,0);
				fallibility = Math.abs(lastPosition.y - b2body.getPosition().y) - 1;
				b2body.setTransform(new Vector2(b2body.getPosition().x, b2body.getPosition().y - fallibility),0);
				isMoving = false;
			}
		}
		
		if (mDown) {
			if ((int)(beginYpos - b2body.getPosition().y) == 1) {
				b2body.setLinearVelocity(0,0);
				fallibility = Math.abs(lastPosition.y - b2body.getPosition().y - 1);
				b2body.setTransform(new Vector2(b2body.getPosition().x, b2body.getPosition().y + fallibility),0);
				isMoving = false;
			}
		}
	}
	
	public void addCoin() {
		this.coinCount++;
	}
	
	public void addSword() {
		this.swordCount++;
	}
	
	public void removeOneSword() {
		this.swordCount--;
	}
	
	public int getCoinCount() {
		return coinCount;
	}
	
	public int getSwordCount() {
		return swordCount;
	}
	
	// ������ ��� ���-������. ��� ������������ ���������� � CUSTOMSTAGE
	public void setLastPosition() {
		lastPosition.x = b2body.getPosition().x;
		lastPosition.y = b2body.getPosition().y;
	}
	
	public Vector2 getLastPosition() {
		return lastPosition;
	}
	
	public boolean isMoving() {
		return isMoving;
	}
	
	
	public void setBeginXpos() {
		beginXpos = b2body.getPosition().x;
	}

	public void setBeginYpos() {
		beginYpos = b2body.getPosition().y;
	}
	
	public void setVelocity(String direction) {
		if (direction.equals("up"))
			 b2body.setLinearVelocity(0, 3);
		if (direction.equals("down"))
			 b2body.setLinearVelocity(0, -3);
		if (direction.equals("left"))
			 b2body.setLinearVelocity(-3, 0);
		if (direction.equals("right"))
			 b2body.setLinearVelocity(3, 0);
	}
	
	public void setLastClickTime() {
		lastPressedButtonTime = TimeUtils.nanoTime();
	}
	//-----------------------------------------------------
	
	@Override
	public void draw(SpriteBatch batcher) {
        sprite = new Sprite(AssetsLoader.getInstance().getMainHeroImg());
        sprite.setSize(1, 1);
        sprite.setPosition((b2body.getPosition().x - sprite.getWidth()/2), ((b2body.getPosition().y - sprite.getHeight()/2)));
        sprite.draw(batcher);
	}

	@Override
	public void update(float delta) {
		if (!isMoving)
			handleInput(delta);
		if(TimeUtils.nanoTime() - lastPressedButtonTime > Constants.ONE_SECOND) isMoving = false;
		makeOneStep();
	}
	
}
