package Entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

import Helpers.AssetsLoader;
import Helpers.CustomUserData;

public abstract class GameObjects {

	protected Body b2body;
	protected Fixture fixture;
	protected Sprite sprite;
	protected Rectangle rect;
	protected boolean isAlive;
	protected boolean isKilled;
	//protected CustomUserData userData;
	
	public abstract void draw(SpriteBatch batcher);
	public abstract void update(float delta);
	public Body getB2body() {return b2body;}
	public Fixture getFixture() {return fixture;}
	public Sprite getSprite() {return sprite;}
	public float getX() {return b2body.getPosition().x;}
	public float getY() {return b2body.getPosition().y;}
	public boolean isKilled(){return isKilled;};
}
