package Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Helpers.AssetsLoader;

public class WinScreen extends AbstractGameScreen {


    SpriteBatch batch;

	public WinScreen(Game game) {
		super(game);
		batch = new SpriteBatch();
	}
	
	@Override
	public void show() {
		
		
	}

	@Override
	public void render(float delta) {
		 Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		 Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	     batch.begin();
	     batch.draw(AssetsLoader.getInstance().getWinImg(),0,0,640,480);
	     batch.end();
	     if (Gdx.input.justTouched())
	     {
	    	 game.setScreen(new MainMenuScreen(game));
	     }
	    	 
	}

	@Override
	public void resize(int width, int height) {
	
		
	}

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
		
		
	}

	@Override
	public void hide() {
	
		
	}

	@Override
	public void dispose() {
		
		
	}

}
