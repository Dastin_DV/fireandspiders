package Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;

import GameWorld.GameRenderer;
import GameWorld.GameWorld;
import GameWorld.WorldContactListener;
import Helpers.AssetsLoader;
import Helpers.ButtonActor;
import Helpers.Constants;
import Helpers.CustomStage;
import Helpers.WorldInputProcessor;

public class GameScreen extends AbstractGameScreen {
    CustomStage stage;
	TextureAtlas buttons;
	ButtonActor btnForward, btnBack, btnRight, btnLeft, btnBiggerZoom, btnSmallerZoom, btnDefZoom, btnRestart;
	private GameWorld gameWorld;
	private GameRenderer gameRenderer;
	private WorldInputProcessor worldProcessor;
	InputMultiplexer multiplexer = new InputMultiplexer(); 
	public GameScreen(Game game) {
		super(game);
		gameWorld = new GameWorld(game, this);
		gameRenderer = new GameRenderer(gameWorld);
		worldProcessor = new WorldInputProcessor(gameWorld, gameRenderer);
		init();  
	}
	
	public void init() {
		float scrX = Gdx.graphics.getWidth();
	    float scrY = Gdx.graphics.getHeight();
	    float scaling = scrX/1280f;     
	    
	    buttons = new TextureAtlas(Gdx.files.internal("textures/control/control.atlas"));
	    stage = new CustomStage(gameWorld, this, gameRenderer);
	    btnForward = new ButtonActor(buttons.findRegion("arrowUp"), 16f, 5f, scaling, "up");
	    btnBack = new ButtonActor(buttons.findRegion("arrowDown"), 16f, 1f, scaling, "down");
	    btnRight = new ButtonActor(buttons.findRegion("arrowRight"), 18f, 3f, scaling, "right");
	    btnLeft = new ButtonActor(buttons.findRegion("arrowLeft"), 14f, 3f, scaling, "left");
	    btnBiggerZoom = new ButtonActor(buttons.findRegion("zoomIn"), 1f, 10f, scaling, "zoomIn");
	    btnSmallerZoom = new ButtonActor(buttons.findRegion("zoomOut"), 1f, 8f, scaling, "zoomOut");
	    btnDefZoom = new ButtonActor(buttons.findRegion("zoomDefault"), 1f, 12f, scaling, "zoomDef");
	    btnRestart = new ButtonActor(buttons.findRegion("return"), 1f, 6f, scaling, "return");
	    stage.addActor(btnForward);
        stage.addActor(btnBack);
        stage.addActor(btnRight);
        stage.addActor(btnLeft);
        stage.addActor(btnBiggerZoom);
        stage.addActor(btnSmallerZoom);
        stage.addActor(btnDefZoom);
        stage.addActor(btnRestart);
        multiplexer.addProcessor(worldProcessor);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
//		Gdx.input.setInputProcessor(this.worldProcessor);
		Gdx.input.setInputProcessor(multiplexer);
		AssetsLoader.getInstance().getShirSound().play();
	}

	@Override
	public void render(float delta) {
		gameWorld.update(delta);
		gameRenderer.render(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		gameRenderer.resize(width, height);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
