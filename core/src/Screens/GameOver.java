package Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import Helpers.Constants;

public class GameOver extends AbstractGameScreen{

	private Skin gameOverSkin;
	private Image imgBackground;
	private Button btnMenuReturn;
	private Button btnMenuReplay;
	
	public GameOver(Game game) {
		super(game);	
	}
	
	private void rebuildStage () {
		gameOverSkin = new Skin(Gdx.files.internal(Constants.SKIN_GAMEOVER_UI),
				 new TextureAtlas(Constants.TEXTURE_ATLAS_GAMEOVER));
		 Table layerBackground = buildBackgroundLayer();
		 Table layerControls = buildControlsLayer();
		 stage.clear();
		 Stack stack = new Stack();
		 stage.addActor(stack);
		 stack.setSize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		 stack.add(layerBackground);
	     stack.add(layerControls);
	} 
	
	private Table buildBackgroundLayer () {
		Table layer = new Table();
		 // + Background
		imgBackground = new Image(gameOverSkin, "GameOver");
		layer.add(imgBackground);
		layer.debug();
		return layer;	
	}
	
	private Table buildControlsLayer () {
		 Table layer = new Table();
		 layer.right().bottom();

		 btnMenuReturn = new Button(gameOverSkin, "repeat");
		 btnMenuReturn.padRight(50);
		 layer.add(btnMenuReturn);
		 btnMenuReturn.addListener(new ChangeListener() {
			 @Override    
			 public void changed (ChangeEvent event, Actor actor) {
				 onReplayClicked();
				 }
			 });
		 
		 btnMenuReplay = new Button(gameOverSkin, "exit");
		 btnMenuReplay.addListener(new ChangeListener() {
			 @Override    
			 public void changed (ChangeEvent event, Actor actor) {
				 onExitClicked ();
				 }
		 });
		 
		 layer.add(btnMenuReplay);
		 return layer;
	} 
	
	private void onReplayClicked () { 
		game.setScreen(new GameScreen(game));
	}
	
	private void onExitClicked () { 
		game.setScreen(new MainMenuScreen(game));
	}
	
	@Override
	public void show() {
		 stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(),  Gdx.graphics.getHeight()));
		 Gdx.input.setInputProcessor(stage);
		 rebuildStage(); 
	}

	
	@Override
	public void render(float delta) {
		 Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		 Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		 stage.act(delta);
		 stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		 stage.getViewport().update(width, height, true); 
	}

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
		
		
	}

	@Override
	public void hide() {
		 stage.dispose();
		 gameOverSkin.dispose(); 
	}

	@Override
	public void dispose() {
		
		
	}

}
