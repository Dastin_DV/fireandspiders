package Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox; 
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import Helpers.Constants;

public class MainMenuScreen extends AbstractGameScreen {

//	 private Stage stage;
	 private Skin skinCatAndSpiders;
	 // menu
	 private Image imgBackground;
	 private Image imgCoins;
	 private Image imgCat;
	 private Button btnMenuPlay;
	 private float width;
	 private float height;
	 private GameScreen gameScreen = null;
	
	 private void rebuildStage () {
		 width = Gdx.graphics.getWidth();
		 height = Gdx.graphics.getHeight();
		 skinCatAndSpiders = new Skin(Gdx.files.internal(Constants.SKIN_MENU_UI),
				 new TextureAtlas(Constants.TEXTURE_ATLAS_MENU_UI));
	    // build all layers
		 Table layerBackground = buildBackgroundLayer();
		 Table layerObjects = buildObjectsLayer();
		 Table layerControls = buildControlsLayer();
	    // assemble stage for menu screen
		 stage.clear();
		 Stack stack = new Stack();
		 stage.addActor(stack);
		 stack.setSize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		 //stack.add(layerObjects);
		 stack.add(layerBackground);
		 stack.add(layerObjects);
	     stack.add(layerControls);
//	     stage.addActor(layerOptionsWindow);
	} 
	
	private Table buildBackgroundLayer () {
		Table layer = new Table();
		 // + Background
		imgBackground = new Image(skinCatAndSpiders, "background");
		layer.add(imgBackground);
		layer.debug();
		return layer;
	}
	
	private Table buildObjectsLayer () {
		
		Table layer = new Table();
		 // + Coins
		imgCoins = new Image(skinCatAndSpiders, "coins");
		layer.addActor(imgCoins);
		imgCoins.setPosition(20, 2);
		
		// + Cat
		imgCat = new Image(skinCatAndSpiders, "Cat");
		layer.addActor(imgCat);
		imgCat.setPosition(250, 40);
		layer.debug();
		return layer;
	}  
	
	private Table buildControlsLayer () {
		 Table layer = new Table();
		 layer.right().bottom();
		   // + Play Button
		 btnMenuPlay = new Button(skinCatAndSpiders, "play");
		 layer.add(btnMenuPlay);
		 btnMenuPlay.addListener(new ChangeListener() {
			 @Override    
			 public void changed (ChangeEvent event, Actor actor) {
				 onPlayClicked();
				 }
			 });
		 return layer;
	}
	
	private void onPlayClicked () { 
		if (gameScreen == null)
			 game.setScreen(new GameScreen(game));
		 else
			 game.setScreen(this.gameScreen);
//		game.setScreen(new WinScreen(game));
	}
	
	public MainMenuScreen(Game game) {
		super(game);
		
	}
	
	public MainMenuScreen(Game game, GameScreen gameScreen) {
		super(game);
		this.gameScreen = gameScreen;
	}

	 @Override
	 public void render (float deltaTime) {
		 Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		 Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		 stage.act(deltaTime);
		 stage.draw();
//		 Table.debug(stage); 
	 }
	 
	 @Override public void resize (int width, int height) { 
		 stage.getViewport().update(width, height, true); 
	 }
	  
	 @Override public void show () {
		 stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(),  Gdx.graphics.getHeight()));
		 Gdx.input.setInputProcessor(stage);
		 rebuildStage(); 
	 }
	  
	 @Override public void hide () {
		 stage.dispose();
		 skinCatAndSpiders.dispose(); 
	 }
	 
	 @Override public void pause () { } 
}
