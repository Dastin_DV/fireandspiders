package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Helpers.AssetsLoader;
import Helpers.WorldInputProcessor;
import Screens.GameScreen;
import Screens.MainMenuScreen;

public class MyGdxGame extends Game {
	
	GameScreen gameScreen;
	
	@Override
	public void create () {
		setScreen(new MainMenuScreen(this));
	}
	
	@Override
	public void dispose () {
	}
}
