package GameWorld;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import Helpers.AssetsLoader;
import Helpers.Constants;
import Helpers.CustomUserData;
import Helpers.LevelManager;

public class Box2DWorldCreater {

	private final int WALLS_LAYOUT = 2;
    public Box2DWorldCreater(World world)
    {
    	loadWalls(world);
    }
    
    // �������� ��� �������� ����
    private void loadWalls(World world) {
    	BodyDef bdef = new BodyDef();
	    PolygonShape shape = new PolygonShape();
	    FixtureDef fdef = new FixtureDef();
	    Body body;
	     
	    for (MapObject object: LevelManager.whatLVL().getLayers().get(WALLS_LAYOUT).getObjects().getByType(RectangleMapObject.class))
	    {
	        Rectangle rect = ((RectangleMapObject) object).getRectangle();
	        
	        bdef.type = BodyDef.BodyType.StaticBody;
	        bdef.position.set((rect.getX() + rect.getWidth()/2) / Constants.PPM, (rect.getY() + rect.getHeight() / 2)/Constants.PPM);
	        
	        body = world.createBody(bdef);
	        
	        shape.setAsBox((rect.getWidth()/2) / Constants.PPM, (rect.getHeight()/2) / Constants.PPM);
	        fdef.shape = shape;
	        
	        body.createFixture(fdef).setUserData("walls");
	        body.setUserData(new CustomUserData(false, "walls"));
	    }
	    
    }
    
 
}
