package GameWorld;

import java.util.ArrayList;
import java.util.LinkedList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import Entities.Coin;
import Entities.MainHero;
import Entities.Spider;
import Entities.Sword;
import Entities.Treasure;
import Helpers.AssetsLoader;
import Helpers.Constants;
import Helpers.CustomUserData;
import Helpers.LevelManager;
import Helpers.WorldInputProcessor;
import Screens.GameOver;
import Screens.GameScreen;
import Screens.MainMenuScreen;
import Screens.WinScreen;

public class GameWorld {
    
	private World box2DWorld;
	private MainHero mainHero;
	private Box2DWorldCreater box2DWorldCreater;
	private ArrayList<Spider> spiders;
	private ArrayList<Coin> coins;
	private ArrayList<Sword> swords;
	private Treasure treasure;
	private Game game;
	private GameScreen gameScreen;
	
	public GameWorld(Game game, GameScreen gameScreen) {
		this.game = game;
		this.gameScreen = gameScreen;
		init();
	}
	
	public void init() {
		box2DWorld = new World(new Vector2(0,0), true);
		box2DWorldCreater = new Box2DWorldCreater(box2DWorld);	
		mainHero = new MainHero(box2DWorld);
		spiders = new ArrayList<Spider>();
		coins = new ArrayList<Coin>();
		swords = new ArrayList<Sword>();
		treasure = new Treasure(box2DWorld);
		initSpiders();
		initCoins();
		initSwords();
		box2DWorld.setContactListener(new WorldContactListener(this));
	}
	
	public void update(float delta) {
//		System.out.println("FPS: " + 1/delta);
		winConditions();
		mainHero.update(delta);
		if (!mainHero.isMoving())
			updateSpiders(delta);
		else
			allowSpidersMakeStep();
		System.out.println(mainHero.isMoving());
		box2DWorld.step(delta, 6, 2);
		sweepDeadBodies();
	}

	// ����������� ������� ���������� ������.
	// ����� ������� ������ ���������� ����� + ����� �� ��������.
	private void winConditions() {
		if (mainHero.getCoinCount() == Constants.CURRENT_COIN_COUNT_WIN
				&& mainHero.getB2body().getPosition().epsilonEquals(treasure.getB2body().getPosition(), 0.3f))
		{
			LevelManager.setNextLVL();
			System.out.println("LVL: " + LevelManager.getCurrentLVL());
			if (LevelManager.getCurrentLVL() - 1 == Constants.LEVEL_COUNT)
			{
				LevelManager.setCurrentLVL(1);
				game.setScreen(new WinScreen(game));
			}
				Gdx.input.getInputProcessor().keyDown(Keys.R);	
		}
	}
	
	private void sweepDeadBodies() {
        Array<Body> bodyList = new Array<Body>();
        box2DWorld.getBodies(bodyList);
        for (int i = 0; i < box2DWorld.getBodyCount(); i++)
        {
            Body body = bodyList.get(i);
            if (body != null) {
                CustomUserData data = (CustomUserData) body.getUserData();

                if ((data.getIsFlaggedForDelete() && ("sword".equals(data.getName())) ||
                		data.getIsFlaggedForDelete() && "coin".equals(data.getName()))) {
                    if (!box2DWorld.isLocked())
                    {   
                            box2DWorld.destroyBody(body);
                            body.setUserData(null);
                            body = null;
                    }
                }
                
                if ("spider".equals(data.getName()) && data.getIsFlaggedForDelete())
                        {
		                	 if (!box2DWorld.isLocked())
		                     { 
	                            box2DWorld.destroyBody(body);
	                            body.setUserData(null);
	                            body = null;
		                     }
                        }
            }
        }
    }

	private void initSpiders() {
        for (MapObject object: LevelManager.whatLVL().getLayers().get(3).getObjects().getByType(RectangleMapObject.class))
        {
             Rectangle rect = ((RectangleMapObject) object).getRectangle();
             spiders.add(new Spider(box2DWorld, rect, mainHero));
        }

	}
	
	private void initCoins() {
        for (MapObject object: LevelManager.whatLVL().getLayers().get(5).getObjects().getByType(RectangleMapObject.class))
        {
             Rectangle rect = ((RectangleMapObject) object).getRectangle();
             coins.add(new Coin(box2DWorld, rect, mainHero));
        }   
        
        Constants.CURRENT_COIN_COUNT_WIN = coins.size();
	}
	
	private void initSwords() {
        for (MapObject object: LevelManager.whatLVL().getLayers().get(4).getObjects().getByType(RectangleMapObject.class))
        {
             Rectangle rect = ((RectangleMapObject) object).getRectangle();
             swords.add(new Sword(box2DWorld, rect, mainHero));
        }

	}
	
	private void allowSpidersMakeStep() {
		for (int i = 0; i < spiders.size(); i++)
			spiders.get(i).allowMakeStep();

	}
	
	private void updateSpiders(float delta) {
		 for (int i = 0; i < spiders.size(); i++) {
			
			if (!spiders.get(i).isKilled())
			{
				if (!spiders.get(i).isMadeStep()) {
					if (spiders.get(i).checkForGameOver())
					{
						game.setScreen(new GameOver(game));
						AssetsLoader.getInstance().getShirSound().stop();
					}
					spiders.get(i).update(delta);
					if (!spiders.get(i).isActive()) // ���������� ������ ���� ��� ��� ��������� �����. ����� ����������������� �� ����� �� ��������� �������, � ������� �������� ��� �� ����������� � ������.
						spiders.get(i).checkForMHActivities();
				}
			}
			else
			{
				spiders.remove(i);
                i--;
			}
			
		 }
		 
		 // ������� ������ ���� ���, ���� ��� ��������������� ���� ��������. ����� ������� ����� ������ ����� ����� ����� �����.	
		 if (Spider.getDeleteFact())
			{
				mainHero.removeOneSword();
				Spider.resetDeleteFact();
			}
	}
	
	public void backToMenu() {
	    game.setScreen(new MainMenuScreen(game, this.gameScreen));
	}
	
	public World getBox2DWorld() {
		return box2DWorld;
	}

	public ArrayList<Spider> getSpiders() {
		return spiders;
	}

	public ArrayList<Coin> getCoins() {
		return coins;
	}
	
	public ArrayList<Sword> getSwords() {
		return swords;
	}
	
	public MainHero getMainHero() {
		return mainHero;
	}

	public Treasure getTreasure() {
		return treasure;
	}
	
	public Box2DWorldCreater getBox2DWorldCreater() {
		return box2DWorldCreater;
	}	
	
}
