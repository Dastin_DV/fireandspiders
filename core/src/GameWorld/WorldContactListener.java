package GameWorld;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.WorldManifold;

import Helpers.AssetsLoader;
import Helpers.CustomUserData;

public class WorldContactListener implements ContactListener{

	private GameWorld world;
	
	public WorldContactListener(GameWorld world){
		this.world = world;
	}
	
	@Override
	public void beginContact(Contact contact) {


        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();
        
        if (fixA.getUserData() == "main" && (fixB.getUserData() == "sword" || fixB.getUserData() == "coin"))
        {
            CustomUserData userData = (CustomUserData) fixB.getBody().getUserData();
            userData.setDeleteFlagTrue();
            fixB.getBody().setUserData(userData);
        }  
        
        if (fixA.getUserData() == "main" & (fixB.getUserData() == "walls"))
        {
        	
        	System.out.println("HELLO");
           world.getMainHero().toAllowMovement();
        }
        //world.getMainHero().toAllowMovement();

	}

	@Override
	public void endContact(Contact contact) {
	
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		WorldManifold manifold = contact.getWorldManifold();
    	for(int j = 0; j < manifold.getNumberOfContactPoints(); j++){	 
                   if(contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("coin")
                		   || contact.getFixtureB().getUserData().equals("exit")
                		   || contact.getFixtureB().getUserData().equals("sword")
                		   || contact.getFixtureB().getUserData().equals("spider") 
                		   || contact.getFixtureB().getUserData().equals("treasure")))
                   {
                	   contact.setEnabled(false);
                   }
                   
                   if (contact.getFixtureB().getUserData()!= null && (contact.getFixtureB().getUserData().equals("coin")))
                   {
                	   world.getMainHero().addCoin();
                	   AssetsLoader.getInstance().getCoinSound().play();
                   }
                   
                   if (contact.getFixtureB().getUserData()!= null && (contact.getFixtureB().getUserData().equals("sword")))
                   {
                	   world.getMainHero().addSword();
                	   AssetsLoader.getInstance().getSwordSound().play();
                   }

    	}
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		//world.getMainHero().toAllowMovement();
		// TODO Auto-generated method stub
		
	}

}
