package GameWorld;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

import Entities.Coin;
import Entities.MainHero;
import Entities.Spider;
import Entities.Sword;
import Entities.Treasure;
import Helpers.AssetsLoader;
import Helpers.Constants;
import Helpers.LevelManager;

public class GameRenderer {

	private GameWorld world;
	private OrthographicCamera cam;
	private OrthographicCamera GUIcam;
	private float screenWidth;
	private float screenHeight;
	private OrthogonalTiledMapRenderer mapRenderer;
	private float unitScale;
	private Box2DDebugRenderer b2dr;
	private SpriteBatch batcher;
	private float defaultCamZoom;
	private float maxCamZoom = 0.5f;
	private float minCamZoom = 3f;
	// ������� ��� ����������
	private MainHero mainHero;
	private ArrayList<Spider> spiders;
	private ArrayList<Coin> coins;
	private ArrayList<Sword> swords;
	private Treasure treasure;
	private boolean firstTimeResize = true;
	public GameRenderer(GameWorld world) {
		this.world = world;
		init();
	}
	
	public void init() {
		unitScale = 1/Constants.PPM;
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		cam = new OrthographicCamera(Constants.WORLD_WIDTH / 4, Constants.WORLD_HEIGHT / 4 * (screenHeight / screenWidth));
		defaultCamZoom = cam.zoom;
		cam.update();
		
        GUIcam = new OrthographicCamera(Constants.WORLD_WIDTH / 4, Constants.WORLD_HEIGHT / 4 * (screenHeight / screenWidth));
        GUIcam.setToOrtho(true);
        GUIcam.update();

		mapRenderer = new OrthogonalTiledMapRenderer(LevelManager.whatLVL(), unitScale);
		mapRenderer.setView(cam);
		b2dr = new Box2DDebugRenderer();
		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(cam.combined);
		
		this.mainHero = world.getMainHero();
		this.spiders = world.getSpiders();
		this.coins = world.getCoins();
		this.swords = world.getSwords();
		this.treasure = world.getTreasure();
	}
	
	private void handleInput() {
		
		if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
			cam.zoom += 0.2;
			if (cam.zoom >= minCamZoom)
				cam.zoom = minCamZoom;
		}
		
		if (Gdx.input.isKeyPressed(Input.Keys.E)) {
			cam.zoom -= 0.2;
			if (cam.zoom <= maxCamZoom)
				cam.zoom = maxCamZoom;
		}
		
		if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
			cam.zoom = defaultCamZoom;
		}

		cam.position.set(world.getMainHero().getX(), world.getMainHero().getY(), 0);
	}
	
		public void render(float delta) {
	    Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		handleInput();
		mapRenderer.setView(cam);
		cam.update();
		mapRenderer.render(); 
		
		batcher.begin();
		batcher.setProjectionMatrix(cam.combined);
		mainHero.draw(batcher);
		treasure.draw(batcher);
		drawSpiders();
		drawCoins();
		drawSwords();
		drawGUI();
		batcher.end();
		
//		b2dr.render(world.getBox2DWorld(), cam.combined);
	}
	
	public void setDefaultZoom() {
		cam.zoom = defaultCamZoom;
	}
		
	private void drawGUI () {
       batcher.setProjectionMatrix(GUIcam.combined);
       AssetsLoader.getInstance().getTextStyle().draw(batcher, "SWORDS: " + mainHero.getSwordCount(), screenWidth / 5 , 30);
       AssetsLoader.getInstance().getTextStyle().draw(batcher, "COINS: " + mainHero.getCoinCount(), screenWidth - (screenWidth / 3), 30);
       GUIcam.update();
    } 

	private void drawSpiders() {
		for (int i = 0; i < spiders.size(); i++)
			spiders.get(i).draw(batcher);
	}
	
	private void drawCoins() {
		for (int i = 0; i < coins.size(); i++)
			coins.get(i).draw(batcher);
	}
	
	private void drawSwords() {
		for (int i = 0; i < swords.size(); i++)
			swords.get(i).draw(batcher);
	}
	
	public OrthographicCamera getCam() {
		return cam;
	}
	
	public void resize(int width, int height) {
		if (!firstTimeResize)
		{
		 cam.viewportWidth =  (Constants.WORLD_HEIGHT / 4 / height) * width;
		 cam.update(); 
		 batcher.setProjectionMatrix(cam.combined);
		}
		firstTimeResize = false;
	}
	
}
