package Helpers;

import com.badlogic.gdx.maps.tiled.TiledMap;

public class LevelManager {

	private static int CURRENT_LVL = 1;

	public static TiledMap whatLVL() {
		switch(CURRENT_LVL) {
		case 1: 
			return AssetsLoader.getInstance().getFirstLevelMap();
		case 2:
			return AssetsLoader.getInstance().getSecondLevelMap();
		}
		return null;
	}
	
	public static void setNextLVL() {
		if (CURRENT_LVL <= Constants.LEVEL_COUNT)
			CURRENT_LVL++;			
		AssetsLoader.getInstance().getMagicSound().play();
	}
	
	public static void setCurrentLVL(int LVL) {
		CURRENT_LVL = LVL;
	}
	
	public static int getCurrentLVL() {
		return CURRENT_LVL;
	}
	
	private static TiledMap getLVL1() {
		return AssetsLoader.getInstance().getFirstLevelMap();	
	}
	
	private static TiledMap getLVL2() {
		return AssetsLoader.getInstance().getSecondLevelMap();	
	}
	
}
