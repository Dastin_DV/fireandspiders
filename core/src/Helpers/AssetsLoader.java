package Helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Disposable;

public class AssetsLoader implements Disposable{

	private static AssetsLoader loader = new AssetsLoader();
	private TiledMap map1;
	private TiledMap map2;
	private Texture mainHeroTexture;
	private Texture spiderTexture;
	private Texture setTexture;
	private Texture winTexture;

	private TextureRegion coinRegion;
	private TextureRegion swordRegion;
	private TextureRegion mainHeroRegion;
	private TextureRegion spiderRegion;
	private TextureRegion treasureRegion;
	private TextureRegion winRegion;
	
	private Sound deadSpider;
	private Sound coinSound;
	private Sound spiderActive;
	private Sound magicSound;
	private Sound shirSound;
	private Sound swordSound;
    
	private final BitmapFont defaultSmall;

	private AssetsLoader() {
		defaultSmall = new BitmapFont( Gdx.files.internal("fonts/arial-15.fnt"), true); 
		 // enable linear texture filtering for smooth fonts
		defaultSmall.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		map1 = new TmxMapLoader().load("FirstLevel.tmx");
		map2 = new TmxMapLoader().load("Level2.tmx");
		mainHeroTexture =  new Texture(Gdx.files.internal("textures/Cat.png"));
		mainHeroRegion = new TextureRegion(mainHeroTexture,83,61,133,156);
		spiderTexture = new Texture(Gdx.files.internal("textures/Spiders.png"));
		winTexture = new Texture(Gdx.files.internal("textures/Win.png"));
		spiderRegion = new TextureRegion(spiderTexture,0,2,34,30);
		setTexture =  new Texture(Gdx.files.internal("textures/Item set.png"));
		swordRegion = new TextureRegion(setTexture,334,0,37,57);
		coinRegion = new TextureRegion(setTexture,202,202,47,41);
		treasureRegion = new TextureRegion(setTexture,196,77,55,48);
		winRegion = new TextureRegion(winTexture,0,0,640,480);

        deadSpider = Gdx.audio.newSound(Gdx.files.internal("sounds/dead.wav"));
        coinSound = Gdx.audio.newSound(Gdx.files.internal("sounds/coin.mp3"));
        spiderActive = Gdx.audio.newSound(Gdx.files.internal("sounds/SpiderIsActive.mp3"));
        magicSound =  Gdx.audio.newSound(Gdx.files.internal("sounds/Magic.mp3"));  
        shirSound = Gdx.audio.newSound(Gdx.files.internal("sounds/Shir.mp3"));
        swordSound = Gdx.audio.newSound(Gdx.files.internal("sounds/sword.mp3"));

	}
	
	public static AssetsLoader getInstance() {
		return loader;
	}
	
	public BitmapFont getTextStyle() {
		return defaultSmall;
	}
	
	public TiledMap getFirstLevelMap() {
		return map1;
	}
	
	public TiledMap getSecondLevelMap() {
		return map2;
	}
	
	public TextureRegion getMainHeroImg() {
		return mainHeroRegion;	
	}
	
	public TextureRegion getSpiderImg() {
		return spiderRegion;
	}
	
	
	public TextureRegion getCoinImg() {
		return coinRegion;
	}

	public TextureRegion getSwordImg() {
		return swordRegion;
	}

	public TextureRegion getTreasureImg() {
		return treasureRegion;
	}

	public TextureRegion getWinImg() {
		return winRegion;
	}
	
	public Sound getDeadSpiderSound() {
		return deadSpider;
	}

	public Sound getCoinSound() {
		return coinSound;
	}

	public Sound getMagicSound() {
		return magicSound;
	}

	public Sound getShirSound() {
		return shirSound;
	}

	public Sound getSpiderActiveSound() {
		return spiderActive;
	}

	public Sound getSwordSound() {
		return swordSound;
	}

	@Override
	public void dispose() {
		map1.dispose();
	}
}
