package Helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Entities.MainHero;
import GameWorld.GameRenderer;
import GameWorld.GameWorld;
import Screens.GameScreen;

public class CustomStage extends Stage{

	private int scrX = Gdx.graphics.getWidth();
    private int scrY = Gdx.graphics.getHeight();
    private MainHero mainHero;
    private GameWorld gameWorld;
    private GameScreen gameScreen;
    private GameRenderer gameRenderer;
	private String aName;
	
	public CustomStage(GameWorld world, GameScreen gameScreen, GameRenderer gameRenderer)
	{
		this.mainHero = world.getMainHero();
		this.gameWorld = world;
		this.gameScreen = gameScreen;
		this.gameRenderer = gameRenderer;
	}
	
	 @Override
	 public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		 this.mainHero = gameWorld.getMainHero();
         screenY = scrY - screenY;
         Actor actor = hit(screenX, screenY, true);
         if(actor == null){
             aName = "null";
             return false;
         }else{
             aName = actor.getName();
             if(aName.equals("up")){
            	 System.out.println("up");
                 mainHero.setUp();
                 mainHero.setBeginYpos();
                 mainHero.setLastPosition();
                 mainHero.setVelocity(aName);
                 mainHero.setLastClickTime();
             }
             if(aName.equals("down")){
            	 System.out.println("down");
                 mainHero.setDown();
                 mainHero.setBeginYpos();
                 mainHero.setLastPosition();
                 mainHero.setVelocity(aName);
                 mainHero.setLastClickTime();
             }
             if(aName.equals("right")){
            	 System.out.println("right");
                mainHero.setRight();
                mainHero.setBeginXpos();
                mainHero.setLastPosition();
                mainHero.setVelocity(aName);
                mainHero.setLastClickTime();
             }
             if(aName.equals("left")){
            	System.out.println("left");
                mainHero.setLeft();
                mainHero.setBeginXpos();
                mainHero.setLastPosition();
                mainHero.setVelocity(aName);
                mainHero.setLastClickTime();
             }
             if(aName.equals("return")){
                Gdx.input.getInputProcessor().keyDown(Keys.R);
                gameScreen.init();
             }
             if(aName.equals("zoomIn")) {
            	 if (gameRenderer.getCam().zoom <= 2)
            		 gameRenderer.getCam().zoom+=0.5f;
             }
             if(aName.equals("zoomOut")) {
            	 if (gameRenderer.getCam().zoom >= 1)
            	 gameRenderer.getCam().zoom-=0.5f;
             }
             if(aName.equals("zoomDef")) {
            	gameRenderer.setDefaultZoom();
             }
             return true;
         }
     }
	 
     @Override
     public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    	 return true;
     }
     
     @Override
     public boolean touchDragged(int screenX, int screenY, int pointer) {
         return false;
     }
     
     @Override
     public Actor hit(float x, float y, boolean touchable) {
         Actor  actor  = super.hit(x,y,touchable);
         return actor;
     }
}
