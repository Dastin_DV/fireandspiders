package Helpers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

import GameWorld.GameRenderer;
import GameWorld.GameWorld;

public class WorldInputProcessor extends InputAdapter{
	
	GameWorld world;
	GameRenderer renderer;
	
	public WorldInputProcessor(GameWorld world, GameRenderer renderer)
	{
		this.world = world;
		this.renderer = renderer;
	}
	
	public boolean keyDown(int keycode) {
		switch(keycode)
		{
			case Keys.R:
				world.getBox2DWorld().dispose();
				world.init();
				renderer.init();		
				break;
			case Keys.ESCAPE:
				world.backToMenu();
				AssetsLoader.getInstance().getShirSound().stop();
				break;
		}
		return true;
	}
	
}
