package Helpers;

public class CustomUserData {

    private boolean isFlaggedForDelete;
    private String name;
    
    public CustomUserData(boolean isFlaggedForDelete, String name){
        this.isFlaggedForDelete = isFlaggedForDelete;
        this.name = name;
    }
    
    public void setDeleteFlagTrue()
    {
        isFlaggedForDelete = true;
    }

    public boolean getIsFlaggedForDelete() {
        return isFlaggedForDelete;
    }

    public String getName() {
        return name;
    }
    
}
