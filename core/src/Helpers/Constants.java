package Helpers;

public class Constants {
	public static final float WORLD_WIDTH = 25;
	public static final float WORLD_HEIGHT = 24;
	public static final float PPM = 32;
	public static final int LEVEL_COUNT = 2;
	public static int CURRENT_COIN_COUNT_WIN;
	public static final String TEXTURE_ATLAS_MENU_UI ="textures/ui/menu-ui.pack.atlas";
	public static final String TEXTURE_ATLAS_GAMEOVER = "textures/gameOver/gameOver.atlas";
	public static final String TEXTURE_ATLAS_SETTINGS ="textures/ui/uiskin.atlas";
	// Location of description file for skins
	public static final String SKIN_MENU_UI ="textures/ui/menu-ui.json";
	public static final String SKIN_SETTINGS_UI ="textures/ui/uiskin.json"; 
	public static final String SKIN_GAMEOVER_UI = "textures/gameOver/gameOver.json";
	public static final Double ONE_SECOND = 1500000000d;
}
